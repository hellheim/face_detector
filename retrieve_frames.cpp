#include "header/retrieve_frames.hpp"
#include "header/frame_size.hpp"
#include "header/error_.hpp"
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>


void retrieve_frames(std::vector<cv::Mat> *frames, int client_sockfd)
{
    config_struct config_;
    char buffer[12];
    int n, nframes, m = 0;
    bool found = false;
    cv::Mat img = cv::Mat::zeros(HEIGHT , WIDTH, CV_8UC3);
    int imgSize = frame_size(CV_8UC3);
    uchar sockData[imgSize];
    bzero(sockData,sizeof(sockData));
    bzero(buffer,sizeof(buffer));
    n = read(client_sockfd,buffer,sizeof(buffer));
    if(n <0)
    {
        error_("Unable to read from socket.." );
    }
    std::cout << "buffer: " << buffer << std::endl;
    while((m < strlen(buffer)) && !found)
    {
        if(buffer[m] == '-')
            found = true;
        else
            m++;
    }
    nframes = std::stoi(std::string(buffer).substr(0,m),nullptr,10);
    std::cout << "Recieving "<< nframes <<" frames.." << std::endl;
    bzero(buffer,sizeof(buffer));
    strcpy(buffer,SEND_STREAM);
    std::cout << "buffer: " << buffer << std::endl;
    n = send(client_sockfd,buffer,sizeof(buffer),0);
    n = 0;
    for(int i = 0; i < nframes; i++)
    {
        bzero(sockData,sizeof(sockData));
        bzero(buffer,sizeof(buffer));
        for(int j = 0; j < imgSize; j += n)
        {
            n = recv(client_sockfd,sockData + j, imgSize - j,0);
            if(n < 0)
            {
                error_("Unable to read from socket..");
            }
        }
        img.data = sockData;
        cv::Mat tmp = img.clone();
        frames->push_back(tmp);
        if(i < nframes-1)
        {
            strcpy(buffer,SEND_STREAM);
            send(client_sockfd,buffer,sizeof(buffer),0);
        }
    }
    bzero(buffer,sizeof(buffer));
    strcpy(buffer,STOP_STREAM);
    send(client_sockfd,buffer,sizeof(buffer),0);
    std::cout << "End recieving.." << std::endl;
}
