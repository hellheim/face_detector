#include "header/retrieve_frame_lc.hpp"

void retrieve_frames_lc(std::vector<cv::Mat> *frames, int nbr)
{
    raspicam::RaspiCam_Cv acquisition;
    acquisition.set(CV_CAP_PROP_FRAME_WIDTH, WIDTH);
    acquisition.set(CV_CAP_PROP_FRAME_HEIGHT, HEIGHT);
    acquisition.set(CV_CAP_PROP_FORMAT,CV_8UC3);
    acquisition.open();
    cv::Mat frame;
    if(acquisition.isOpened())
    {
        std::cout << "Camera opened.." << std::endl;
        for(int i = 0; i < nbr; i++)
        {
            acquisition.grab();
            acquisition.retrieve(frame);
            cv::Mat tmp = frame.clone();
            frames->push_back(tmp);
        }
        acquisition.release();
    }
    else 
    {
        std::cout << "Unable to open raspicam.." << std::endl;
    }
}