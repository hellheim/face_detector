#ifndef RETRIEVE_FRAME_LC_HPP
#define RETRIEVE_FRAME_LC_HPP

#include <opencv2/opencv.hpp>
#include <raspicam/raspicam_cv.h>
#include "config_struct.hpp"
#include "config_socket.hpp"
#include <stdio.h>

#define SEND_STREAM "send_stream"
#define ACCEPT_STREAM "streaming_"
#define STOP_STREAM "stop_stream"

#define WIDTH 640
#define HEIGHT 480

void retrieve_frames_lc(std::vector<cv::Mat> *frames, int nbr);

#endif