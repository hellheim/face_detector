#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP
#include <iostream>
#include <string>
#include <vector>
#include <opencv2/core/mat.hpp>
#include <numeric>

#define AVG_TIME 0
#define AVG_THROUGHPUT 1 
#define SPEED_UP 2
#define DEFAULT_ALGO "algo-"

class Algorithm
{
    protected:
        std::string name;
        std::vector<float> stat_time;
        std::vector<float> stat_throughput;
        float stat_avg_time;
        float stat_avg_throughput;
        float speed_up;
    public:
        Algorithm();
        Algorithm(std::string name, float stat_avg_time, float stat_avg_throughput, float speed_up);
        std::string getName();
        std::vector<float> getStatTime();
        std::vector<float> getStatThroughput();
        float getStatAvg(int label);
        void doSatistics();
        virtual void algorithm(std::vector<cv::Mat> input, std::vector<cv::Mat> *output) = 0;
        void convert(std::vector<cv::Mat> input, std::vector<cv::UMat> *output);
        void display();
        ~Algorithm();
};

#endif