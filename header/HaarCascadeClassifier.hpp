#ifndef HAARCASCADECLASSIFIER_HPP
#define HAARCASCADECLASSIFIER_HPP

#include "Algorithm.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>

class HaarCascadeClassifier: public Algorithm
{
    public:
        HaarCascadeClassifier();
        void algorithm(std::vector<cv::Mat> input, std::vector<cv::Mat> *output);
        ~HaarCascadeClassifier();
};

#endif