#ifndef CONFIG_SOCKET_HPP
#define CONFIG_SOCKET_HPP

#include "config_struct.hpp"

void config_socket(config_struct_local *config_, const char * socket_name , int *client_sockfd);

#endif