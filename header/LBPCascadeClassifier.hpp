#ifndef LBPCASCADECLASSIFIER_HPP
#define LBPCASCADECLASSIFIER_HPP

#include "Algorithm.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>

class LBPCascadeClassifier: public Algorithm
{
    public:
        LBPCascadeClassifier();
        void algorithm(std::vector<cv::Mat> input, std::vector<cv::Mat> *output);
        ~LBPCascadeClassifier();
};

#endif