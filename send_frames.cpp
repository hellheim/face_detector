#include "header/send_frames.hpp"
#include "header/frame_size.hpp"
#include "header/error_.hpp"
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>


void send_frames(std::vector<cv::Mat> *frames, int serv_sockfd, const char *nof)
{
    char buffer[12];
    int client_sockfd, n, nframes = std::stoi(nof), width_, height_,l=0;
    cv::Mat img;
    cv::Size size_tmp;
    bool quit = false;
    int imgSize = frame_size(CV_8UC3);
    uchar sockData[imgSize];
    bzero(sockData,sizeof(sockData));
    bzero(buffer,sizeof(buffer));
    bzero(buffer,sizeof(buffer));
    char tmp_nof[5];
    sprintf(tmp_nof,"%d",frames->size());
    strcpy(buffer,tmp_nof);
    n = write(serv_sockfd,buffer,strlen(buffer));
    if(n < 0)
    {
        error_("Unable to recieve");
    }
    bzero(buffer,sizeof(buffer));
    n = read(serv_sockfd,buffer,sizeof(buffer));
    if(n < 0)
    {
        error_( "Unable to read from socket");
    }
    
    if(strcmp(buffer,SEND_STREAM)==0)
    {
        while(!quit)
        {
            bzero(buffer,sizeof(buffer));
            img = frames->at(l).clone();
            size_tmp = img.size();
            width_ = size_tmp.width;
            height_ = size_tmp.height;
            img = img.reshape(0,1);
            imgSize = img.total()*img.elemSize();
            //sending width
            strcpy(buffer,std::to_string(width_).c_str());
            n = write(serv_sockfd,buffer, strlen(buffer));
            if(n < 0)
                error_("ERROR sending width to target..");
            /******************************/
            //waiting
            bzero(buffer,sizeof(buffer));
            n = read(serv_sockfd,buffer,sizeof(buffer));
            if(strcmp(buffer,SEND_STREAM) != 0)
                error_("ERROR in matching protocol with target..");
            /******************************/
            //sending height
            bzero(buffer,sizeof(buffer));            
            strcpy(buffer,std::to_string(height_).c_str());
            n = write(serv_sockfd,buffer, strlen(buffer));
            if(n < 0)
                error_("ERROR sending width to target..");
            /******************************/
            //waiting
            bzero(buffer,sizeof(buffer));
            n = read(serv_sockfd,buffer,sizeof(buffer));
            if(strcmp(buffer,SEND_STREAM) != 0)
                error_("ERROR in matching protocol with target..");
            /******************************/
            //sending imgSize
            bzero(buffer,sizeof(buffer));            
            strcpy(buffer,std::to_string(imgSize).c_str());
            n = write(serv_sockfd,buffer, strlen(buffer));
            if(n < 0)
                error_("ERROR sending width to target..");
            /******************************/
            //waiting
            bzero(buffer,sizeof(buffer));
            n = read(serv_sockfd,buffer,sizeof(buffer));
            if(strcmp(buffer,SEND_STREAM) != 0)
                error_("ERROR in matching protocol with target..");
            /******************************/
            /****** sending image *******/
            n = send(serv_sockfd,img.data,imgSize,0);
            if(n < 0)
                error_("ERROR sending image to target..");
            bzero(buffer,sizeof(buffer));
            n = read(serv_sockfd,buffer,sizeof(buffer));
            if(strcmp(buffer,SEND_STREAM) == 0)
            {
                l++;
                std::cout << ">";
            }
            else
            {
                quit = true;
                std::cout << "stop sending.." << std::endl;
            }
        }
    }
}
