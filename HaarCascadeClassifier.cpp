#include "header/HaarCascadeClassifier.hpp"
#include "header/frame_size.hpp"

using namespace cv;

HaarCascadeClassifier::HaarCascadeClassifier():Algorithm("HaarCascadeClassifier",0.0,0.0,0.0)
{
}
void HaarCascadeClassifier::algorithm(std::vector<cv::Mat> input, std::vector<cv::Mat> *output)
{
    //*** defining global variables
    String face_cascade_name = "haar_features/haarcascade_frontalface_alt2.xml";
    CascadeClassifier face_cascade;
    std::vector<int> imgSize;
    std::vector<Rect> outputs;
    Mat gray_frame, frameROI;
    int t1,t2;
    //*** testing xml training file loads
    if(!face_cascade.load(face_cascade_name))
    {
        std::cout << "Error loading " << face_cascade_name << std::endl;
        exit(-1);
    }
    std::cout << std::endl << "Start analyzing" << std::endl;
    for(size_t f_indx = 0; f_indx < input.size(); f_indx++)
    {
        std::cout << "*";
        outputs.clear();
        imgSize.push_back(frame_size(input.at(f_indx).type()));
        cvtColor(input.at(f_indx),gray_frame, CV_BGR2GRAY);
        equalizeHist(gray_frame,gray_frame);
        t1 = getTickCount();
        face_cascade.detectMultiScale(gray_frame, outputs, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30,30));
        for(size_t j = 0; j < outputs.size(); j++)
        {
            frameROI = gray_frame(outputs[j]);
            Mat tmp = frameROI.clone();
            output->push_back(tmp);
        }
        if(!(outputs.empty()))
        {
            t2 = getTickCount();
            this->stat_time.push_back((t2 - t1)/getTickFrequency());
            std::cout << this->stat_time[f_indx] << std::endl;
            this->stat_throughput.push_back(imgSize[f_indx]/this->stat_time[f_indx]);
        }
    }
    std::cout <<"\n" <<std::endl;
}
HaarCascadeClassifier::~HaarCascadeClassifier()
{
    
}