#include "header/Algorithm.hpp"

Algorithm::Algorithm()
{
    this->name = DEFAULT_ALGO;
    this->speed_up = 0.0;
    this->stat_avg_throughput = 0.0;
    this->stat_avg_time = 0.0;
}

Algorithm::Algorithm(std::string name, float stat_avg_time, float stat_avg_throughput, float speed_up)
{
    this->name = name;
    this-> stat_avg_time = stat_avg_time;
    this->stat_avg_throughput = stat_avg_throughput;
    this->speed_up = this->speed_up;
}

std::string Algorithm::getName()
{
    return this->name;
}

std::vector<float> Algorithm::getStatTime()
{
    return this->stat_time;
}

std::vector<float> Algorithm::getStatThroughput()
{
    return this->stat_throughput;
}

float Algorithm::getStatAvg(int label)
{
    switch(label)
    {
        case AVG_TIME:
            return stat_avg_time;
        case AVG_THROUGHPUT:
            return stat_avg_throughput;
        case SPEED_UP:
            return speed_up;
        default:
            return 0.0;
    }
}

void Algorithm::doSatistics()
{
    this->stat_avg_time = accumulate(this->stat_time.begin() + 1,this->stat_time.end(),0.0) / this->stat_time.size();
    this->stat_avg_throughput = accumulate(this->stat_throughput.begin() + 1,this->stat_throughput.end(),0.0) / this->stat_throughput.size();
}

void Algorithm::display() 
{
    this->doSatistics();
    std::cout << this->name << std::endl;
    std::cout << "Average resolution time per frame: " << this->stat_avg_time << " sec."<< std::endl;
    std::cout << "Average throughput per frame: " << this->stat_avg_throughput<< " Bps."<< std::endl;
}

void Algorithm::convert(std::vector<cv::Mat> input, std::vector<cv::UMat> *output)
{
    for(size_t i =0; i< input.size(); i++)
    {
        cv::UMat tmp;
        input.at(i).copyTo(tmp);
        output->push_back(tmp);
    }
}


Algorithm::~Algorithm()
{
    
}