set( RPI_ROOTFS /rpi/root-rpi )
set( CMAKE_FIND_ROOT_PATH ${RPI_ROOTFS} )
# compilers
set( CMAKE_C_COMPILER   "/usr/bin/arm-linux-gnueabihf-gcc"    CACHE FILEPATH "")
set( CMAKE_CXX_COMPILER "/usr/bin/arm-linux-gnueabihf-g++"    CACHE FILEPATH "")
set( CMAKE_AR           "/usr/bin/arm-linux-gnueabihf-ar"     CACHE FILEPATH "")
set( CMAKE_RANLIB       "/usr/bin/arm-linux-gnueabihf-ranlib" CACHE FILEPATH "")

# Platform
set( CMAKE_SYSTEM_NAME Linux )
set( CMAKE_SYSTEM_VERSION 1 )
set( CMAKE_SYSTEM_PROCESSOR arm )
set( CMAKE_LIBRARY_ARCHITECTURE arm-linux-gnueabihf )
set( FLOAT_ABI_SUFFIX "hf" )
add_definitions( "-mfpu=vfp -mfloat-abi=hard -marm" )

# setup RPI include/lib/pkgconfig directories for compiler/pkgconfig
set( RPI_INCLUDE_DIR "-std=c++11 ${RPI_INCLUDE_DIR} -isystem ${RPI_ROOTFS}/usr/include/arm-linux-gnueabihf")
set( RPI_INCLUDE_DIR "${RPI_INCLUDE_DIR} -isystem ${RPI_ROOTFS}/usr/include")
set( RPI_INCLUDE_DIR "${RPI_INCLUDE_DIR} -isystem ${RPI_ROOTFS}/usr/local/include")

set( RPI_LIBRARY_DIR "${RPI_LIBRARY_DIR} -Wl,-rpath-link,${RPI_ROOTFS}/usr/lib/arm-linux-gnueabihf")
set( RPI_LIBRARY_DIR "${RPI_LIBRARY_DIR} -Wl,-rpath-link,${RPI_ROOTFS}/lib/arm-linux-gnueabihf")
set( RPI_LIBRARY_DIR "${RPI_LIBRARY_DIR} -Wl,-rpath-link,${RPI_ROOTFS}/usr/lib")

set( RPI_PKGCONFIG_LIBDIR "${RPI_PKGCONFIG_LIBDIR}:${RPI_ROOTFS}/usr/lib/arm-linux-gnueabihf/pkgconfig" )
set( RPI_PKGCONFIG_LIBDIR "${RPI_PKGCONFIG_LIBDIR}:${RPI_ROOTFS}/usr/share/pkgconfig" )
set( RPI_PKGCONFIG_LIBDIR "${RPI_PKGCONFIG_LIBDIR}:${RPI_ROOTFS}/opt/vc/lib/pkgconfig" )
set( RPI_PKGCONFIG_LIBDIR "${RPI_PKGCONFIG_LIBDIR}:/home/pi/ros/src_cross/devel_isolated" )

# C/CXX flags
set( CMAKE_CXX_FLAGS        "-mfpu=vfp -mfloat-abi=hard -marm -pthread ${CMAKE_CXX_FLAGS} ${RPI_INCLUDE_DIR}" CACHE STRING "" FORCE)
set( CMAKE_C_FLAGS          "-mfpu=vfp -mfloat-abi=hard -marm -pthread ${CMAKE_C_FLAGS} ${RPI_INCLUDE_DIR}" CACHE STRING "" FORCE)
set( CMAKE_EXE_LINKER_FLAGS "-mfpu=vfp -mfloat-abi=hard -marm -pthread ${CMAKE_EXE_LINKER_FLAGS} ${RPI_LIBRARY_DIR}" CACHE STRING "" FORCE)

# Pkg-config settings
set( PKG_CONFIG_EXECUTABLE "/usr/bin/pkg-config" CACHE FILEPATH "")
set( ENV{PKG_CONFIG_DIR}         "" CACHE FILEPATH "")
set( ENV{PKG_CONFIG_LIBDIR}      "${RPI_PKGCONFIG_LIBDIR}" CACHE FILEPATH "")
set( ENV{PKG_CONFIG_SYSROOT_DIR} "${RPI_ROOTFS}" CACHE FILEPATH "")

# Boost
set( BOOST_LIBRARYDIR "${RPI_ROOTFS}/usr/lib/arm-linux-gnueabihf" CACHE STRING "")


# Python2.7
set( PYTHON_EXECUTABLE          "/usr/bin/python2.7" CACHE STRING "")
set( PYTHON_LIBRARY_DEBUG       "${RPI_ROOTFS}/usr/lib/arm-linux-gnueabihf/libpython2.7.so" CACHE STRING "")
set( PYTHON_LIBRARY_RELEASE     "${RPI_ROOTFS}/usr/lib/arm-linux-gnueabihf/libpython2.7.so" CACHE STRING "")
set( PYTHON_LIBRARY             "${RPI_ROOTFS}/usr/lib/arm-linux-gnueabihf/libpython2.7.so" CACHE STRING "")
set( PYTHON_INCLUDE_DIR         "${RPI_ROOTFS}/usr/include/python2.7" CACHE STRING "")
set( PYTHON2_NUMPY_INCLUDE_DIRS "${RPI_ROOTFS}/usr/lib/python2.7/dist-packages/numpy/core/include" CACHE STRING "")
set( PYTHON2_PACKAGES_PATH      "${RPI_ROOTFS}/usr/local/lib/python2.7/site-packages" CACHE STRING "")

# OpenCV
set( OpenCV_DIR       "${RPI_ROOTFS}/usr/share/OpenCV/" CACHE STRING "")

#Raspicam
SET(raspicam_INCLUDE_DIRS ${RPI_ROOTFS}/usr/local/include )
#LINK_DIRECTORIES("${RPI_ROOTFS}/usr/local/lib")

SET(raspicam_LIBS ${RPI_ROOTFS}/opt/vc/lib/libmmal_core.so;${RPI_ROOTFS}/opt/vc/lib/libmmal_util.so;${RPI_ROOTFS}/opt/vc/lib/libmmal.so raspicam) 
SET(raspicam_FOUND "YES") 

SET(raspicam_CV_FOUND  "YES")
SET(raspicam_CV_LIBS ${RPI_ROOTFS}/opt/vc/lib/libmmal_core.so;${RPI_ROOTFS}/opt/vc/lib/libmmal_util.so;${RPI_ROOTFS}/opt/vc/lib/libmmal.so;${RPI_ROOTFS}/usr/lib/libraspicam.so opencv_calib3d;opencv_core;opencv_dnn;opencv_features2d;opencv_flann;opencv_highgui;opencv_imgcodecs;opencv_imgproc;opencv_ml;opencv_objdetect;opencv_photo;opencv_shape;opencv_stitching;opencv_superres;opencv_video;opencv_videoio;opencv_videostab;opencv_aruco;opencv_bgsegm;opencv_bioinspired;opencv_ccalib;opencv_datasets;opencv_dnn_objdetect;opencv_dpm;opencv_face;opencv_freetype;opencv_fuzzy;opencv_hfs;opencv_img_hash;opencv_line_descriptor;opencv_optflow;opencv_phase_unwrapping;opencv_plot;opencv_reg;opencv_rgbd;opencv_saliency;opencv_stereo;opencv_structured_light;opencv_surface_matching;opencv_text;opencv_tracking;opencv_xfeatures2d;opencv_ximgproc;opencv_xobjdetect;opencv_xphoto /${RPI_ROOTFS}/usr/lib/libraspicam_cv.so)

