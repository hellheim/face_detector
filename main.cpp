#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>
#include <iostream>
#include <thread>
#include "header/Algorithm.hpp"
#include "header/HaarCascadeClassifier.hpp"
#include "header/LBPCascadeClassifier.hpp"
#include "header/error_.hpp"
#include "header/retrieve_frames.hpp"
#include "header/send_frames.hpp"
#include "header/config_socket.hpp"
#include "header/config_socket_send.hpp"
#include "header/initi_pcontrol.hpp"
#include "header/sig_handler.hpp"
#include "header/sigint_listner.hpp"
#include "header/get_trigger.hpp"
#include "header/retrieve_frame_lc.hpp"

using namespace std;

int fd_sig_handler = 0;

int main(int argc, char **argv)
{
    if(argc < 3)
        error_("Please use like: face_detector <socket-send> <socket-control>");
    cv::ocl::setUseOpenCL(true);
    vector<cv::Mat> stream_frames;
    bool siglistner = false;
    bool_tr control_trigger = notsend;
    //vector<cv::UMat> stream_frames_u;
    vector<cv::Mat> detected_frames;
    config_struct_local config_,config_send, config_control;
    Algorithm *test_object;
    int nof, client_sockfd, counter_protection = 0;
    char file_name[20],buffer[12];
    test_object = new LBPCascadeClassifier();
    //*socket_name = argv[1];
    char  *socket_name_2 = argv[1], *socket_control = argv[2];
    //configuring local connections
    config_socket_send(&config_control,socket_control);
    fd_sig_handler = config_control.sockfd;
    signal(SIGINT,sig_handler);
    initi_pcontrol(config_control.sockfd);
    //thread worker(sigint_listner,config_control.sockfd,&siglistner);
    //config_socket(&config_,socket_name,&client_sockfd); //---> used for recieving frames from video_capture
    config_socket_send(&config_send,socket_name_2);
    //sending routine
    while(!siglistner)
    {
        control_trigger = get_trigger(config_control.sockfd);
        if(control_trigger == sends)
        {
            cout << control_trigger << endl;
            retrieve_frames_lc(&stream_frames,3);
            //retrieve_frames(&stream_frames,client_sockfd);
            //test_object->convert(stream_frames,&stream_frames_u);
            test_object->algorithm(stream_frames, &detected_frames);
            test_object->doSatistics();
            test_object->display();
            nof = static_cast<int>(detected_frames.size());
            string Nof = std::to_string(nof);
            send_frames(&detected_frames, config_send.sockfd,Nof.c_str());
            stream_frames.clear();
            detected_frames.clear();
            control_trigger = notsend;
        }
        else if(control_trigger == stop)
        {
            bzero(buffer, sizeof(buffer));
            strcpy(buffer,EXIT);
            write(config_send.sockfd, buffer, sizeof(buffer));
            siglistner = true;
        }
        else if (control_trigger == notsend)
        {
            counter_protection++;
            if(counter_protection > 5)
            {
                bzero(buffer, sizeof(buffer));
                strcpy(buffer,EXIT);
                write(config_send.sockfd, buffer, sizeof(buffer));
                siglistner = true;
            }
        }
    }
    return 0;
}
